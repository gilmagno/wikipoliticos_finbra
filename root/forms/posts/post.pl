use utf8;
{
    languages => ['pt_br'],
    elements => [
        { type => 'Hidden',
          name => 'id' },
        { type => 'Text',
          name => 'title',
          label => 'Título',
          constraints => 'Required',
          attrs => { class => 'form-control'} },
        
        { type => 'Text',
          name => 'uri',
          label => 'Endereço da fonte',
          attrs => { class => 'form-control'} },

        { type => 'Select',
          name => 'politician_tokens',
          label => 'Políticos relacionados, caso haja algum',
          attrs => { multiple => 'multiple', class => 'form-control', id => 'post-related-politicians' } },

        { type => 'Textarea',
          name => 'body',
          label => 'Texto da postagem',
          attrs => { class => 'form-control'} },
        
        { type => 'Text',
          name => 'captcha',
          constraints => 'Required',
          label => 'Validação: digite o número na imagem acima',
          attrs => { class => 'form-control'} },
        
        { type => 'Submit',
          name => 'submit',
          value => 'Enviar',
          attrs => { class => 'btn btn-primary' } }
   ],

   model_config => {resultset => 'Post' },
   auto_container_class => 'form-group',
}
