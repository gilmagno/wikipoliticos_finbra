package WikiPoliticos::Web;
use Moose;
use namespace::autoclean;

use Catalyst::Runtime 5.80;

# Set flags and add plugins for the application.
#
# Note that ORDERING IS IMPORTANT here as plugins are initialized in order,
# therefore you almost certainly want to keep ConfigLoader at the head of the
# list if you're using it.
#
#         -Debug: activates the debug mode for very useful log messages
#   ConfigLoader: will load the configuration from a Config::General file in the
#                 application's home directory
# Static::Simple: will serve static files from the application's root
#                 directory

use Catalyst qw/
    -Debug
    ConfigLoader
    Static::Simple
    StackTrace
    Authentication
    Session
    Session::Store::File
    Session::State::Cookie
    Captcha
/;
#ErrorCatcher

extends 'Catalyst';

our $VERSION = '0.01';

__PACKAGE__->config(
    name => 'WikiPoliticos::Web',
    # Disable deprecated behavior needed by old applications
    disable_component_resolution_regex_fallback => 1,
    enable_catalyst_header => 1, # Send X-Catalyst header

    'Plugin::Authentication' => {
        default => {
            credential => {
                class => 'Password',
                password_field => 'password',
                password_type => 'hashed',
                password_hash_type => 'SHA-1',
            },
            store => {
                class => 'DBIx::Class',
                user_model => 'DB::User',
            },
        },
    },
    
    'Plugin::Captcha'  => {
        session_name => 'captcha_string',
        new => {
            width => 90,
            height => 50,
            ptsize => '26',
            font => q{/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf},
            rndmax => 3,
        },
        create => ['ttf', 'ec', '#0066CC', '#0066CC'],
        particle => [50],
        out => {force => 'jpeg'},
    },
);

__PACKAGE__->setup();

1;
