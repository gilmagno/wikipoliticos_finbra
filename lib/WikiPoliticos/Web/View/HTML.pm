package WikiPoliticos::Web::View::HTML;
use Moose;
use namespace::autoclean;
use Number::Format qw/format_number/;

extends 'Catalyst::View::TT';

__PACKAGE__->config(
    TEMPLATE_EXTENSION => '.tt',
    render_die => 1,
    ENCODING => 'UTF-8',
    WRAPPER => 'wrapper.tt',
    expose_methods => [qw/format_money/],
);

sub format_money {
    my ($self, $c, $number) = @_;
    my $number_formater = Number::Format->new(-thousands_sep => '.',
                                              -decimal_point => ',');
    return $number_formater->format_number($number, 2, 2);
}


=head1 NAME

WikiPoliticos::Web::View::HTML - TT View for WikiPoliticos::Web

=head1 DESCRIPTION

TT View for WikiPoliticos::Web.

=head1 SEE ALSO

L<WikiPoliticos::Web>

=head1 AUTHOR

sgrs,,,

=head1 LICENSE

This library is free software. You can redistribute it and/or modify
it under the same terms as Perl itself.

=cut

1;
