package WikiPoliticos::Web::Controller::Posts;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base :Chained('/') PathPart('h') CaptureArgs(0) {
    my ( $self, $c ) = @_;
    $c->stash(posts_rs => $c->model('DB::Post'));
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $id ) = @_;
    my $post = $c->stash->{posts_rs}->find($id);
    $c->stash(post => $post);
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;

    my $query = $c->req->params->{'q'} || '';
    my $page = $c->req->params->{'pg'} || '';

    my ($posts, $pager) =
      $c->model('DB::Post')->paginated_search
      ({ query => $query, page => $page });

    $c->stash(posts => $posts, pager => $pager, safe_q => $query =~ s/<|>//gr);
}

sub details :Chained('object') PathPart('') Args(0) {}

sub add_post :Path('/postar') Args(0) {
    my ( $self, $c ) = @_;

    use HTML::FormFu;
    my $form = HTML::FormFu->new
      ({ load_config_file => 'root/forms/posts/post.pl' });
    $form->stash->{schema} = $c->model('DB')->schema;
    $form->process( $c->req->params );

    if ($form->submitted_and_valid) {
        $form->model->create;
    }
    else {
        $c->log->debug('else')
    }

    $c->stash(form => $form);
}

sub search_politician :Chained('base') PathPart('search-politician') Args(0) {
    my ( $self, $c ) = @_;

    my $politicians_ids_texts = [];

    my $query = $c->req->params->{'q'};

    if ($query) {
        use Text::Unidecode;
        $query = unidecode $query;
        $query = lc $query;
        $query =~ s/\s+/%/g;

        my @politicians = $c->model('DB::Politician')->search(
            { token => { ilike => '%' . $query . '%' } },
            { order_by => { -asc => 'name' }, rows => '100'});
        $politicians_ids_texts = [
            map { { id => $_->token, text => $_->name_party } } @politicians
        ];
    }

    use JSON;
    $c->res->content_type("application/json");
    $c->res->body( encode_json $politicians_ids_texts );
}

sub search_location :Chained('base') PathPart('search-location') Args(0) {
    my ( $self, $c ) = @_;

    my $locations_ids_texts = [];

    my $query = $c->req->params->{'q'};

    if ($query) {
        use Text::Unidecode;
        $query = unidecode $query;
        $query = lc $query;
        $query =~ s/\s+/%/g;

        my @locations = $c->model('DB::Location')->search(
            { id => { ilike => '%' . $query . '%' } },
            { order_by => [ { -asc => 'search_relevance' },
                            { -asc => 'name'            } ],
              rows => '100' },
        );
        $locations_ids_texts = [
            map { { id => $_->id, text => $_->name } } @locations
        ];
    }

    use JSON;
    $c->res->content_type("application/json");
    $c->res->body( encode_json $locations_ids_texts );
}

__PACKAGE__->meta->make_immutable;

1;
