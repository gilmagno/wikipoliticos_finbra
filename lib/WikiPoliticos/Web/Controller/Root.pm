package WikiPoliticos::Web::Controller::Root;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

__PACKAGE__->config(namespace => '');

sub index :Path :Args(0) {
    my ( $self, $c ) = @_;
}

sub login :Path('/entrar') Args(0) {
    my ( $self, $c ) = @_;

    if ($c->req->method eq 'POST') {
        my $username = $c->req->params->{username};
        my $password = $c->req->params->{password};

        if ( $c->authenticate({ token => $username, password => $password }) ) {
            $c->res->redirect( $c->uri_for_action('/index') );
        }
        else {
            # TODO flash msg
            $c->res->redirect( $c->uri_for_action('/login') );
        }
    }
}

sub logout :Path('/sair') Args(0) {
    my ( $self, $c ) = @_;
    $c->logout;
    $c->res->redirect( $c->uri_for_action('/index') );
}

sub captcha :Path('/captcha') Args(0) {
    my ( $self, $c ) = @_;
    $c->create_captcha;
}

sub default :Path {
    my ( $self, $c ) = @_;
    $c->response->body( 'Page not found' );
    $c->response->status(404);
}

sub end : ActionClass('RenderView') {}

__PACKAGE__->meta->make_immutable;

1;
