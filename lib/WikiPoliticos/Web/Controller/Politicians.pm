package WikiPoliticos::Web::Controller::Politicians;
use Moose;
use namespace::autoclean;
use List::MoreUtils qw/any/;

BEGIN { extends 'Catalyst::Controller' }

sub base :Chained('/') PathPart('p') CaptureArgs(0) {
    my ( $self, $c ) = @_;
    $c->stash(politicians_rs => $c->model('DB::Politician'));
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $token ) = @_;
    my $politician = $c->stash->{politicians_rs}->find($token, { prefetch => ['candidatures'] });
    $c->stash(politician => $politician);
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    my $query = $c->req->params->{'q'} || '';
    $query =~ s/<|>//g;
    my $page = $c->req->params->{'pg'} || '';
    my ($politicians, $pager);
    if ($query) {
        ($politicians, $pager) = $c->model('DB::Politician')->paginated_search({ query => $query, page => $page });
    }

    my @candidatures_presidente;
    my @candidatures_presidente_tmp = $c->model('DB::Candidature')->search({ political_position => 'Presidente' },
                                                                       { prefetch => 'politician',
                                                                         order_by => { -asc => 'politician_id' } });
    my @candidatures_presidente_politicians_tokens;

    # remove duplicates
    for my $cand (@candidatures_presidente_tmp) {
        if ( ! any { $cand->politician->token eq $_ } @candidatures_presidente_politicians_tokens ) {
            push @candidatures_presidente_politicians_tokens, $cand->politician->token;
            push @candidatures_presidente, $cand;
        }
    }
    
    my @candidatures_governador;
    my @candidatures_governador_tmp = $c->model('DB::Candidature')->search({ political_position => 'Governador' },
                                                                       { prefetch => 'politician',
                                                                         order_by => [ { -asc => 'state' },
                                                                                       { -asc => 'politician_id' } ] });
    my @candidatures_governador_politicians_tokens;

    # remove duplicates
    for my $cand (@candidatures_governador_tmp) {
        if ( ! any { $cand->politician->token eq $_ } @candidatures_governador_politicians_tokens ) {
            push @candidatures_governador_politicians_tokens, $cand->politician->token;
            push @candidatures_governador, $cand;
        }
    }
    
    my %candidatures_governador_by_state;
    for (@candidatures_governador) { push @{ $candidatures_governador_by_state{$_->state} }, $_ }
    my @ordered_states = sort keys %candidatures_governador_by_state;

    $c->stash(candidatures_presidente => \@candidatures_presidente,
              candidatures_governador_by_state => \%candidatures_governador_by_state,
              ordered_states => \@ordered_states,
              politicians => $politicians,
              pager => $pager,
              safe_q => $query);
}

sub details :Chained('object') PathPart('') Args(0) {}

sub posts :Chained('object') PathPart('postagens') Args(0) {
    my ( $self, $c ) = @_;
    my @posts = $c->stash->{politician}->posts(undef, { order_by => { -desc => 'created' } });
    $c->stash(posts => \@posts);
}

sub financing_2014 :Chained('object') PathPart('financiamento-eleitoral-2014') Args(0) {
    my ( $self, $c ) = @_;
    my $politician = $c->stash->{politician};
    my @donations = $c->model('DB::CandidateDonation2014')->search
      ({ candidato_cpf => $politician->cpf }, { order_by => { -desc => 'receita_valor' }, prefetch => ['doador_cnpjf'] });
    my $donations_sum = sum_donations(@donations);
    $c->stash(year => 2014,
              donations => \@donations,
              donations_sum => $donations_sum,
              template => 'politicians/financing.tt');
}

sub financing_2012 :Chained('object') PathPart('financiamento-eleitoral-2012') Args(0) {
    my ( $self, $c ) = @_;
    my $politician = $c->stash->{politician};
    my @donations = $c->model('DB::CandidateDonation2012')->search
      ({ candidato_cpf => $politician->cpf }, { order_by => { -desc => 'receita_valor' }, prefetch => ['doador_cnpjf']  });
    my $donations_sum = sum_donations(@donations);
    $c->stash(year => 2012,
              donations => \@donations,
              donations_sum => $donations_sum,
              template => 'politicians/financing.tt');
}

sub financing_2010 :Chained('object') PathPart('financiamento-eleitoral-2010') Args(0) {
    my ( $self, $c ) = @_;
    my $politician = $c->stash->{politician};
    my @donations = $c->model('DB::CandidateDonation2010')->search
      ({ candidato_cpf => $politician->cpf }, { order_by => { -desc => 'receita_valor' }, prefetch => ['doador_cnpjf']  });
    my $donations_sum = sum_donations(@donations);
    $c->stash(year => 2010,
              donations => \@donations,
              donations_sum => $donations_sum,
              template => 'politicians/financing.tt');
}

sub sum_donations {
    my @donations = @_;
    my $sum;
    $sum += $_->receita_valor for @donations;
    return $sum;
}

__PACKAGE__->meta->make_immutable;

1;
