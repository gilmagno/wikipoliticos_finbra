package WikiPoliticos::Web::Controller::Locations;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base :Chained('/') PathPart('l') CaptureArgs(0) {
    my ( $self, $c ) = @_;
    $c->stash(locations_rs => $c->model('DB::Location'));
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $token ) = @_;
    my $location = $c->stash->{locations_rs}->find($token);
    $c->stash(location => $location);
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    my $query = $c->req->params->{'q'} || '';
    my $page = $c->req->params->{'pg'} || '';
    my ($locations, $pager);
    if ($query) {
        ($locations, $pager) = $c->model('DB::Location')->paginated_search({ query => $query, page => $page });
    }
    $c->stash(locations => $locations, pager => $pager, safe_q => $query =~ s/<|>//gr);
}

sub details :Chained('object') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
}

sub public_budget :Chained('object') PathPart('orcamento-publico') Args(0) {
    my ( $self, $c ) = @_;
    my $location = $c->stash->{location};

    my @despesas_funcao_objs = $location->finbra_despesas_funcaos
      (undef, { order_by => { -asc => 'finbra_funcao.nome' }, prefetch => ['finbra_funcao'] });

    my $despesas_funcao_sum;
    $despesas_funcao_sum += $_->valor for @despesas_funcao_objs;

    my @despesas_funcao;
    for (@despesas_funcao_objs) {
        my %despesa;
        $despesa{funcao_codigo} = $_->finbra_funcao->funcao_codigo;
        $despesa{funcao_nome}   = $_->finbra_funcao->nome;
        $despesa{valor}         = $_->valor;
        $despesa{porcentagem}   = $despesa{valor} / $despesas_funcao_sum * 100;
        push @despesas_funcao, \%despesa;
    }

    $c->stash(despesas_funcao => \@despesas_funcao, despesas_funcao_sum => $despesas_funcao_sum);
}

__PACKAGE__->meta->make_immutable;

1;
