package WikiPoliticos::Web::Controller::Financiers;
use Moose;
use namespace::autoclean;

BEGIN { extends 'Catalyst::Controller' }

sub base :Chained('/') PathPart('f') CaptureArgs(0) {
    my ( $self, $c ) = @_;
    $c->stash(financiers_rs => $c->model('DB::Financier'));
}

sub object :Chained('base') PathPart('') CaptureArgs(1) {
    my ( $self, $c, $token ) = @_;
    my $financier = $c->stash->{financiers_rs}->find($token);
    $c->stash(financier => $financier);
}

sub index :Chained('base') PathPart('') Args(0) {
    my ( $self, $c ) = @_;
    my $query = $c->req->params->{'q'} || '';
    $query =~ s/<|>//g;
    my $page = $c->req->params->{'pg'} || '';
    my ($financiers, $pager);
    if ($query) {
        ($financiers, $pager) = $c->model('DB::Financier')->paginated_search({ query => $query, page => $page });
    }
    $c->stash(financiers => $financiers, pager => $pager, safe_q => $query);
}

sub details :Chained('object') PathPart('') Args(0) {}

sub financing_2014 :Chained('object') PathPart('financiamento-eleitoral-2014') Args(0) {
    my ( $self, $c ) = @_;
    my $financier = $c->stash->{financier};

    my $attrs = { order_by => { -desc => 'me.receita_valor' } };

    my @candidates_donations = $financier->candidates_donations_2014s(undef, $attrs);
    my @committees_donations = $financier->committees_donations_2014s(undef, $attrs);
    my @parties_donations    = $financier->parties_donations_2014s(undef, $attrs);

    my $candidates_donations_sum;
    my $committees_donations_sum;
    my $parties_donations_sum;

    $candidates_donations_sum += $_->receita_valor for @candidates_donations;
    $committees_donations_sum += $_->receita_valor for @committees_donations;
    $parties_donations_sum    += $_->receita_valor for @parties_donations;

    my $donations_sum = $candidates_donations_sum + $committees_donations_sum + $parties_donations_sum;

    $c->stash(year                     => 2014,
              candidates_donations     => \@candidates_donations,
              committees_donations     => \@committees_donations,
              parties_donations        => \@parties_donations,
              candidates_donations_sum => $candidates_donations_sum,
              committees_donations_sum => $committees_donations_sum,
              parties_donations_sum    => $parties_donations_sum,
              donations_sum            => $donations_sum,
              template                 => 'financiers/financing.tt');
}

sub financing_2012 :Chained('object') PathPart('financiamento-eleitoral-2012') Args(0) {
    my ( $self, $c ) = @_;
    my $financier = $c->stash->{financier};

    my $attrs = { order_by => { -desc => 'me.receita_valor' } };

    my @candidates_donations = $financier->candidates_donations_2012s(undef, $attrs);
    my @committees_donations = $financier->committees_donations_2012s(undef, $attrs);
    my @parties_donations    = $financier->parties_donations_2012s(undef, $attrs);

    my $candidates_donations_sum;
    my $committees_donations_sum;
    my $parties_donations_sum;

    $candidates_donations_sum += $_->receita_valor for @candidates_donations;
    $committees_donations_sum += $_->receita_valor for @committees_donations;
    $parties_donations_sum    += $_->receita_valor for @parties_donations;

    my $donations_sum = $candidates_donations_sum + $committees_donations_sum + $parties_donations_sum;

    $c->stash(year                     => 2012,
              candidates_donations     => \@candidates_donations,
              committees_donations     => \@committees_donations,
              parties_donations        => \@parties_donations,
              candidates_donations_sum => $candidates_donations_sum,
              committees_donations_sum => $committees_donations_sum,
              parties_donations_sum    => $parties_donations_sum,
              donations_sum            => $donations_sum,
              template                 => 'financiers/financing.tt');
}

sub financing_2010 :Chained('object') PathPart('financiamento-eleitoral-2010') Args(0) {
    my ( $self, $c ) = @_;
    my $financier = $c->stash->{financier};

    my $attrs = { order_by => { -desc => 'me.receita_valor' } };

    my @candidates_donations = $financier->candidates_donations_2010s(undef, $attrs);
    my @committees_donations = $financier->committees_donations_2010s(undef, $attrs);
    my @parties_donations    = $financier->parties_donations_2010s(undef, $attrs);

    my $candidates_donations_sum;
    my $committees_donations_sum;
    my $parties_donations_sum;

    $candidates_donations_sum += $_->receita_valor for @candidates_donations;
    $committees_donations_sum += $_->receita_valor for @committees_donations;
    $parties_donations_sum    += $_->receita_valor for @parties_donations;

    my $donations_sum = $candidates_donations_sum + $committees_donations_sum + $parties_donations_sum;

    $c->stash(year                     => 2010,
              candidates_donations     => \@candidates_donations,
              committees_donations     => \@committees_donations,
              parties_donations        => \@parties_donations,
              candidates_donations_sum => $candidates_donations_sum,
              committees_donations_sum => $committees_donations_sum,
              parties_donations_sum    => $parties_donations_sum,
              donations_sum            => $donations_sum,
              template                 => 'financiers/financing.tt');
}

__PACKAGE__->meta->make_immutable;

1;
