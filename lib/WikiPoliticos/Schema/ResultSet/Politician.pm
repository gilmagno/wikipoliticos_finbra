use utf8;
package WikiPoliticos::Schema::ResultSet::Politician;
use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

# Receives $conf->{query} and $conf->{page}
# Returns $politicians and $pager
sub paginated_search {
    my ($self, $conf) = @_;

    my $criteria;
    
    if ($conf->{query}) {
        my $query = lc $conf->{query} =~ s/\s+/%/gr;
        $criteria = { token => { ilike => '%' . $query . '%' } }
    }

    my $page = $conf->{page} || 1;
    
    my $politicians_rs = $self->search_rs(
        $criteria,
        { order_by => [ { -asc => 'me.name'}, { -desc => 'candidatures.year' } ],
          rows => 300,
          page => $page,
          prefetch => ['candidatures'] }
    );

    my @politicians    = $politicians_rs->all;
    my $pager          = $politicians_rs->pager;

    return \@politicians, $pager;
}

1;
