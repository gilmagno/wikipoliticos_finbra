use utf8;
package WikiPoliticos::Schema::ResultSet::Financier;
use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

# Receives $conf->{query} and $conf->{page}
# Returns $financiers and $pager
sub paginated_search {
    my ($self, $conf) = @_;

    my $criteria;
    
    if ($conf->{query}) {
        my $query = lc $conf->{query} =~ s/\s+/%/gr;
        $criteria = { token => { ilike => '%' . $query . '%' } }
    }

    my $page = $conf->{page} || 1;
    
    my $financiers_rs = $self->search_rs(
        $criteria,
        { order_by => { -asc => 'me.name' },
          rows => 300,
          page => $page }
    );

    my @financiers   = $financiers_rs->all;
    my $pager        = $financiers_rs->pager;

    return \@financiers, $pager;
}

1;
