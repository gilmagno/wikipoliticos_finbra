use utf8;
package WikiPoliticos::Schema::ResultSet::Post;
use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

# Receives $conf->{query} and $conf->{page}
# Returns $posts and $pager
sub paginated_search {
    my ($self, $conf) = @_;

    my $criteria;
    
    if ($conf->{query}) {
        my $query = lc $conf->{query} =~ s/\s+/%/gr;
        $criteria = { title => { ilike => '%' . $query . '%' } }
    }

    my $page = $conf->{page} || 1;
    
    my $posts_rs = $self->search_rs(
        $criteria,
        { order_by => { -desc => 'me.created'},
          rows => 300,
          page => $page,
          prefetch => { posts_politicians => { 'politician_token' } } }
    );

    my @posts = $posts_rs->all;
    my $pager = $posts_rs->pager;

    return \@posts, $pager;
}

1;
