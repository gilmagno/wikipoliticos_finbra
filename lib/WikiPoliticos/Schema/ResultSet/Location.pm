use utf8;
package WikiPoliticos::Schema::ResultSet::Location;
use strict;
use warnings;
use base 'DBIx::Class::ResultSet';

# Receives $conf->{query} and $conf->{page}
# Returns $locations and $pager
sub paginated_search {
    my ($self, $conf) = @_;

    my $criteria;
    
    if ($conf->{query}) {
        my $query = lc $conf->{query} =~ s/\s+/%/gr;
        $criteria = { id => { ilike => '%' . $query . '%' } }
    }

    my $page = $conf->{page} || 1;
    
    my $locations_rs = $self->search_rs(
        $criteria,
        { order_by => { -asc => 'me.name'},
          rows => 300,
          page => $page }
    );

    my @locations    = $locations_rs->all;
    my $pager        = $locations_rs->pager;

    return \@locations, $pager;
}

1;
