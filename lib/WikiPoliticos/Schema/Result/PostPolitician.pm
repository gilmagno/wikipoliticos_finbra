use utf8;
package WikiPoliticos::Schema::Result::PostPolitician;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");
__PACKAGE__->table("posts_politicians");
__PACKAGE__->add_columns(
  "post_id",
  { data_type => "integer", is_foreign_key => 1, is_nullable => 0 },
  "politician_token",
  {
    data_type      => "text",
    is_foreign_key => 1,
    is_nullable    => 0,
    original       => { data_type => "varchar" },
  },
);
__PACKAGE__->set_primary_key("post_id", "politician_token");
__PACKAGE__->belongs_to(
  "politician_token",
  "WikiPoliticos::Schema::Result::Politician",
  { token => "politician_token" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);
__PACKAGE__->belongs_to(
  "post",
  "WikiPoliticos::Schema::Result::Post",
  { id => "post_id" },
  { is_deferrable => 0, on_delete => "NO ACTION", on_update => "NO ACTION" },
);


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-24 14:22:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:DlH1rUdQQZvPPIHOd5KeOg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
