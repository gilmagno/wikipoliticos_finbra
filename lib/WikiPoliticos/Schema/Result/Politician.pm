use utf8;
package WikiPoliticos::Schema::Result::Politician;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Core';
__PACKAGE__->load_components("InflateColumn::DateTime", "TimeStamp");
__PACKAGE__->table("politicians");
__PACKAGE__->add_columns(
  "token",
  {
    data_type   => "text",
    is_nullable => 0,
    original    => { data_type => "varchar" },
  },
  "name",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "political_name",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "party",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
  "cpf",
  {
    data_type   => "text",
    is_nullable => 1,
    original    => { data_type => "varchar" },
  },
);
__PACKAGE__->set_primary_key("token");
__PACKAGE__->add_unique_constraint("politicians_cpf_key", ["cpf"]);
__PACKAGE__->has_many(
  "candidates_donations_2010s",
  "WikiPoliticos::Schema::Result::CandidateDonation2010",
  { "foreign.candidato_cpf" => "self.cpf" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "candidates_donations_2012s",
  "WikiPoliticos::Schema::Result::CandidateDonation2012",
  { "foreign.candidato_cpf" => "self.cpf" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "candidates_donations_2014s",
  "WikiPoliticos::Schema::Result::CandidateDonation2014",
  { "foreign.candidato_cpf" => "self.cpf" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "candidatures",
  "WikiPoliticos::Schema::Result::Candidature",
  { "foreign.politician_id" => "self.token" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->has_many(
  "posts_politicians",
  "WikiPoliticos::Schema::Result::PostPolitician",
  { "foreign.politician_token" => "self.token" },
  { cascade_copy => 0, cascade_delete => 0 },
);
__PACKAGE__->many_to_many("posts", "posts_politicians", "post");


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-04-24 14:22:51
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:wEXh8yfNoTONpaUcsAEzLA

sub recent_candidature_str {
    my $self = shift;
    my @candidatures = $self->candidatures;
    @candidatures = sort { $a->year < $b->year } @candidatures;

    my $candidature = $candidatures[0];

    return unless $candidature;

    my @values = ($candidature->year, $candidature->political_position,
                  $candidature->state);
    push @values, $candidature->city
      if $candidature->year eq 2012;

    return join ', ', @values;
}

sub candidature_by_year {
    my ($self, $year) = @_;
    return $self->candidatures({ year => $year })->first;
}

sub has_candidature_on_year {
    my ($self, $year) = @_;
    my $candidature = $self->candidatures({ year => $year })->first;
    return $candidature ? 1 : undef;
}

sub name_party {
    my ($self) = @_;
    return $self->name . ' - ' . $self->party;
}

# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
