use strict;
use warnings;
use WikiPoliticos::Web::Model::DB;
use Data::Dumper;

my $schema = WikiPoliticos::Web::Model::DB->new->schema;
my $csvs_basedir = $ARGV[0];

my $locations_rs = $schema->resultset('Location')->search_rs;
my $finbra_subfuncoes_rs = $schema->resultset('FinbraSubfuncao')->search_rs;
my $finbra_despesas_subfuncao_rs =
  $schema->resultset('FinbraDespesasSubfuncao')->search_rs;

my @finbra_subfuncoes = $finbra_subfuncoes_rs->all;
my @finbra_subfuncoes_codigos = map { $_->subfuncao_codigo } @finbra_subfuncoes;
# Deixando codigos em ordem crescente
@finbra_subfuncoes_codigos = sort { $a <=> $b } @finbra_subfuncoes_codigos;

open my $fh, '<:encoding(utf8)', $csvs_basedir;

my @column_names = split /,/, <$fh>;

while (my $line = <$fh>) {
    #next unless $line =~ /^23,440/;
    my ($location, $subfuncoes_despesas) = process_line($line, @column_names);

    # my @codes  = (1031,           1032,   1999,   2061,  2062,  2999 );
    my @values = (111159527.2300, 0.0000,         0.0000,

                  0.000,          0.000,          0.000,

                  3409590.9700,   23058172.8100,  240753.2700,

                  4366446.5300,   393936714.3200, 0.0000, 1572933.7600,
                  7958629.6200,   29349317.5000,  0.000,  378653.1300,
                  7155842.7000,   0.000,          0.000,  0.000,
                 );
    # my %hash;
    # @hash{ @codes } = @values;

    # while (my ($key, $value) = each %hash) {
    #     if ($value != $subfuncoes_despesas->{$key}) {
    #         print "errado ";
    #         print "$key $value ";
    #         print $subfuncoes_despesas->{$key};
    #         print "\n";
    #     }
    # }

    # last;

    while (my ($subfuncao_id, $despesa) = each %{$subfuncoes_despesas}) {
        my $despesa_to_create = { ano => 2012,
                                  location_id => $location->id,
                                  finbra_subfuncao_id => $subfuncao_id,
                                  valor => $despesa };

        $finbra_despesas_subfuncao_rs->create($despesa_to_create);
    }
}

close $fh or die "$fh: $!";

sub process_line {
    my ($line, @column_names) = @_;
    my @split_line = split /,/, $line;
    my $estado_codigo = $split_line[0];
    my $municipio_codigo = $split_line[1];
    my %subfuncoes_despesas;

    my $location = $locations_rs->find
      ({ ibge_estado_codigo => $estado_codigo,
         ibge_municipio_codigo => $municipio_codigo });

    my @finbra_subfuncoes_despesas;

    for my $i (4 .. $#column_names) {
        push @finbra_subfuncoes_despesas, $split_line[$i];

        if ($column_names[$i] =~ /^Demais Sub/) { $i += 2 }
        else { $i++ }
    }

    # print "@finbra_funcoes_codigos \n";
    #print "@finbra_funcoes_despesas \n";

    @subfuncoes_despesas{ @finbra_subfuncoes_codigos } = @finbra_subfuncoes_despesas;

    #print Dumper \%funcoes_despesas;

    return $location, \%subfuncoes_despesas;
}
